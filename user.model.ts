import { Model } from './model';

export class User extends Model {
  id: number;
  fullname: string;
  firstname: string;
  surname: string;
  salutation: string;
  initials: string;
  infix: string;
  email: string;
  phone_number: string;
  validation: string;
  is_blocked: boolean;
  company_id: number;
  role: number;
  role_title: string;
}

export enum UserTypes {
  SalutationTypeUnknown = '',
  SalutationTypeMr = 'Mr',
  SalutationTypeMrs = 'Mrs',

  RoleAdmin = 2,
  RoleHSE = 3,
}
