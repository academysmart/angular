import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import * as FileSaver from 'file-saver';

import { environment } from '../../../environments/environment';
import { LoggerService } from './logger.service';
import { SSP } from '../../models/ssp.model';
import { User } from '../../models/user.model';
import { ValidationService } from './validation.service';
import { Validation, ValidationTypes } from '../../models/validation.model';

@Injectable()
export class UserService {
  constructor(private http: HttpClient,
              private logger: LoggerService,
              private validationService: ValidationService) { }

  search(ssp: SSP): Observable<any> {
    this.logger.debug('call search users endpoint');

    return this.http.get<any>(`${environment.apiUrl}/users`,
      {
        params: {
          page: (ssp.getPage().pageNumber).toString(),
          per_page: ssp.getPage().perPage.toString(),
          search: ssp.getSearch(),
          sort: ssp.getSort().sort,
          direction: ssp.getSort().direction
        }
      });
  }

  getById(id: number): Observable<User> {
    this.logger.debug('call get user by id endpoint');

    return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
  }

  create(data: User): Observable<any> {
    this.logger.debug(`call create endpoint`);

    return this.http.post<any>(`${environment.apiUrl}/users`, data);
  }

  update(id: number, data: User): Observable<any> {
    this.logger.debug(`call update user ${id} endpoint`);

    return this.http.put<any>(`${environment.apiUrl}/users/${id}`, data);
  }

  delete(id: number): Observable<any> {
    this.logger.debug(`call delete user ${id} endpoint`);

    return this.http.delete<any>(`${environment.apiUrl}/users/${id}`);
  }

  validate(id: number): Observable<any> {
    return this.getById(id).
      pipe(
        switchMap((user: User) => {
          const validation = new Validation();

          validation.access_type = ValidationTypes.AccessTypeAllowed;
          validation.validation_type = ValidationTypes.ValidationTypeEmail;
          validation.validation_reference = user.email;
          validation.description = user.email;

          return this.validationService.create(validation);
        })
      );
  }

  exportExcel(fileName: string, ssp: SSP): void {
    this.logger.debug('call export users endpoint');

    this.http.get(`${environment.apiUrl}/users/export`,
      {
        responseType: 'blob',
        params: {
          page: (ssp.getPage().pageNumber).toString(),
          per_page: ssp.getPage().perPage.toString(),
          search: ssp.getSearch(),
          sort: ssp.getSort().sort,
          direction: ssp.getSort().direction
        }
      })
      .subscribe(data => {
        FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + '.xlsx');
      });
  }
}
