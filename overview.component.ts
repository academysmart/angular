import { Component, OnInit } from '@angular/core';

import { finalize, first } from 'rxjs/operators';
import { ConfirmationService, MessageService } from 'primeng/api';

import { SSP } from '../../../models/ssp.model';
import { User } from '../../../models/user.model';
import { Sort } from '../../../models/sort.model';
import { Page } from '../../../models/page.model';
import { AuthenticationService, UserService } from '../../../core/services';
import { Identity } from '../../../models/identity.model';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  ssp: SSP;
  user: Identity;
  users: User[];
  loadingIndicator: boolean;

  constructor(private userService: UserService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService,
              private authenticationService: AuthenticationService) {
    this.ssp = new SSP('', new Sort('', ''), new Page(0, 20));

    this.users = [];
  }

  ngOnInit() {
    this.user = this.authenticationService.currentUser;

    this.setPage({offset: 0});
  }

  onSearch(value: string) {
    this.ssp.setSearch(value);

    this.getUsers();
  }

  onExport() {
    const fileName = 'users';

    this.userService.exportExcel(fileName, this.ssp);
  }

  onValidate(id: number) {
    this.loadingIndicator = true;

    this.userService.validate(id)
      .pipe(
        first(),
        finalize(() => this.loadingIndicator = false)
      )
      .subscribe(() => {
        this.messageService.add({severity: 'success', summary: 'Success', detail: 'User was validated'});

        this.getUsers();
      });
  }

  onDelete(id: number) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this user?',
      accept: () => {
        this.userService.delete(id)
          .pipe(
            first()
          )
          .subscribe(
            () => {
              this.messageService.add({severity: 'success', summary: 'Success', detail: 'User was deleted'});

              this.getUsers();
            });
      }
    });
  }

  setPage(pageInfo) {
    this.ssp.setPageNumber(pageInfo.offset);

    this.getUsers();
  }

  setSort(event) {
    const sort = event.sorts[0];

    if (sort.prop !== 'email') {
      return;
    }

    this.ssp.setSortProperty(sort.prop);
    this.ssp.setSortDirection(sort.dir);

    this.getUsers();
  }

  getUsers() {
    this.loadingIndicator = true;

    this.userService.search(this.ssp)
      .pipe(
        first(),
        finalize(() => this.loadingIndicator = false)
      )
      .subscribe(response => {
        this.ssp.setPageTotalElements(response.total);
        this.ssp.setPageTotalPages(response.lastPage);

        this.users = response.data;
      });
  }
}
