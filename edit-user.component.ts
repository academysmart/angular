import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { first, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { ConfirmationService, MessageService } from 'primeng/api';

import { User, UserTypes } from '../../../models/user.model';
import { AuthenticationService, CompanyService, UserService } from '../../../core/services';
import { processErrors } from '../../../helpers/error.helper';
import { Identity } from '../../../models/identity.model';
import { SSP } from '../../../models/ssp.model';
import { Sort } from '../../../models/sort.model';
import { Page } from '../../../models/page.model';
import { Company } from '../../../models/company.model';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  form: FormGroup;
  user: User;
  identity: Identity;
  companies: [];
  submitted: boolean;
  editMode: boolean;

  salutationTypes = [
    { label: 'unknown', value: UserTypes.SalutationTypeUnknown },
    { label: 'Mr', value: UserTypes.SalutationTypeMr },
    { label: 'Mrs', value: UserTypes.SalutationTypeMrs },
  ];

  roleTypes = [
    { label: 'Admin', value: UserTypes.RoleAdmin },
    { label: 'HSE', value: UserTypes.RoleHSE },
  ];

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService,
              private authenticationService: AuthenticationService,
              private companyService: CompanyService) {
  }

  ngOnInit() {
    this.identity = this.authenticationService.currentUser;

    this.route.params
      .pipe(
        map((params: Params) => +params['id']),
        switchMap(id => {
          if (id) {
            this.editMode = true;
            return this.userService.getById(id);
          }

          return of(null);
        }),
        first()
      )
      .subscribe((user: User) => {
        this.user = user;

        this.initForm();
      });

    if (this.identity.is_admin) {
      this.companyService.search(new SSP('', new Sort('', ''), new Page(0, 100)))
        .pipe(
          map(response => {
            return response.data.map((company: Company) => {
              return { label: company.title, value: company.id };
            });
          })
        )
        .subscribe((companies) => this.companies = companies);
    }
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    if (this.editMode) {
      this.userService.update(this.user.id, this.form.value)
        .pipe(first())
        .subscribe(
          () => {
            this.router.navigate(['users']);
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'User was updated'});
          },
          (errors) => {
            processErrors(errors, this.form);
          });
    } else {
      this.userService.create(this.form.value)
        .pipe(first())
        .subscribe(
          () => {
            this.router.navigate(['users']);
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'User was created'});
          },
          (errors) => {
            processErrors(errors, this.form);
          });
    }
  }

  onDelete() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this user?',
      accept: () => {
        this.userService.delete(this.user.id)
          .pipe(first())
          .subscribe(
            () => {
              this.router.navigate(['users']);
              this.messageService.add({severity: 'success', summary: 'Success', detail: 'User was deleted'});
            });
      }
    });
  }

  onBack() {
    this.router.navigate(['users']);
  }

  onChangeRoleType(value: number) {
    if (value === UserTypes.RoleAdmin) {
      this.form.patchValue({
        company_id: null
      });

      this.f['company_id'].setValidators([]);
    } else if (value === UserTypes.RoleHSE) {
      this.form.patchValue({
        company_id: this.user ? this.user.company_id : null
      });

      this.f['company_id'].setValidators(Validators.required);
    }

    this.f['company_id'].updateValueAndValidity();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      salutation : this.user ? this.user.salutation : '',
      initials: this.user ? this.user.initials : '',
      firstname: this.user ? this.user.firstname : '',
      infix: this.user ? this.user.infix : '',
      surname: this.user ? this.user.surname : '',
      phone_number: this.user ? this.user.phone_number : '',
      email: [this.user ? this.user.email : '', [Validators.required, Validators.email]],
      company_id: [this.user ? this.user.company_id : '', Validators.required],
      role: this.user ? this.user.role : UserTypes.RoleHSE
    });
  }
}
